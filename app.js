let kataNumber = 2;
let kataName = `Kata ${kataNumber}: Double Letter Word Splitter`;
document.title = kataName;
document.getElementById("frontPageH1").innerText = kataName;

function splitOnDoubleLetter(aString) {
  if(typeof(aString) !== "string"){
    console.warn("Not of type string!");
    return [];
  }

  const output = [];
  let previousChar;
  for (let charIndex in aString) {
    // Check if current character is equal to previousCharacter
    if (aString[charIndex] === previousChar) {
      // Insert 'splitHere'
      let tempString = aString.substring(0, charIndex);
      tempString += "splitHere" + aString.substring(charIndex);
      // split on 'splitHere'
      let tempArray = tempString.split('splitHere');

      // Add split string to array.
      output.push(tempArray[0]);
      output.push(tempArray[1]); // <-- The remainder of the string. (temporary)
    }
    previousChar = aString[charIndex];
  }

  console.info(output);
}

splitOnDoubleLetter(null); // []
splitOnDoubleLetter("Hello world"); // 'Hel' 'lo world'
splitOnDoubleLetter("Hello letter"); // 'Hel', 'lo let', 'ter'
splitOnDoubleLetter("Mississippi"); // 'Mis', 'sis', 'sip', 'pi'
splitOnDoubleLetter("Hi"); // []
// splitOnDoubleLetter(52);
// splitOnDoubleLetter({});
// splitOnDoubleLetter([]);
// splitOnDoubleLetter(true);